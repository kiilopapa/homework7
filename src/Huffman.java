
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private byte[] data;
   private int inputLength;
   private int[] frequency;
   private LinkedList<Node> nodes;
   private Node tree;
   private HashMap<Byte, String> codetable = new HashMap<Byte, String>();


   // TODO!!! Your instance variables here!

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      data = original;
      inputLength = original.length;
      frequency = new int[255];
      createNodes(original);
      System.out.println(nodes);
      System.out.println("||||||||||||||||");
      tree = createTree();
      createCodeTable(tree);
      System.out.println(codetable.toString());

      // TODO!!! Your constructor here!;
   }

   private string createCodeTable(Node tree) {
      if (tree.character!=Character.MIN_VALUE){
         return
      }
      this.tree
      return null;
   }

   private Node createTree() {
      Node r = null;
      while (nodes.size()>0) {
         Node r1 = nodes.poll();
         r1.one = false;
         //if (codetable.containsKey((byte)r1.character))
         if (r1.character!=Character.MIN_VALUE)
         codetable.put((byte)r1.character, "0");
         Node r2 = nodes.poll();
         r2.one = true;
         if (r2.character!=Character.MIN_VALUE)
            codetable.putIfAbsent((byte)r2.character, "1");
         r = new Node(r1.frequency+r2.frequency, r1, r2 );
         if (nodes.size()==0) return r;
         nodes.add(r);
         Collections.sort(nodes);
         for (Node n: nodes ) {
            System.out.println(n);
         }
         System.out.println("**********************");
         //System.out.println(nodes);
      }
      return r;
   }


   private void createNodes(byte[] original) {
      nodes = new LinkedList<Node>();
      for (byte b : original ) {
         frequency[b]++;
      }
      for (int i = 0; i < frequency.length; i++) {
         if (frequency[i] > 0){
            //System.out.println((char) i + " " + frequency[i]);
            nodes.add(new Node((char) i, frequency[i]));

            //System.out.println(nodes.toString());
         }
      }
      Collections.sort(nodes);

      //for (Node n: nodes ) {
      //   System.out.println(n);
      //}
      //PriorityQueue<Node> noded = new PriorityQueue<Node>(nodes.size(),nodes.comparator());
      //System.out.println("ff" + noded);


   }

   static class Node implements Comparable<Node> {
      private char character = Character.MIN_VALUE;
      private int frequency;
      private Node left = null;
      private Node right = null;
      private boolean one;

      private Node(char character, int frequency) {
         this.character = character;
         this.frequency = frequency;
      }

      private Node(int frequency, Node left, Node right){
         this.frequency = frequency;
         this.left = left;
         this.right = right;
      }


      @Override
      public String toString(){
         return "[" + this.character + " - " + this.frequency +" "+ this.left +" "+ this.right +" "+ this.one +"]";
      }


      @Override
      public int compareTo(Node n) {
         //System.out.println("comparing :" + this.frequency + " with " + n.frequency);
         return this.frequency - n.frequency;
      }
   }

  // private void countFrequency(byte[] original) {
    //  for (byte b : original ) {
      //   frequency[b]++;
        // nodes.put( b , frequency[b] );
         //frequency[b]++;
     // }
   //}

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return 0; // TODO!!!
   }

   public int getInputlength() {
      return inputLength;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {

      return null; // TODO!!!
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      return null; // TODO!!!
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "uuuAlllllAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      System.out.println(huf.inputLength);

      System.out.println(huf.tree);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }
}

